const express = require('express');

//For cross origin request
const cors = require('cors');

// To connect to MongoDB
const mongoose = require('mongoose')

// Environment variables in dotenv file
require('dotenv').config();

// Create an Express server
const app = express();


// It will start the server on port 5000 if available and server will start any of the free port available
const port = process.env.PORT || 5000;

// Cors middleware
app.use(cors());

// Allows us to parse Json
app.use(express.json());


const uri = process.env.ATLAS_URI;
mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

const connection = mongoose.connection;

connection.once('open', () => {
    console.log("MongoDB database connection established successfully")
});

const exercisesRouter = require('./routes/exercises');
const usersRouter = require('./routes/users');

app.use('/exercises', exercisesRouter);
app.use('/users', usersRouter);


// Starts listening server
app.listen(port, () => {
    console.log(`Server is running on port  ${port}`);
});
