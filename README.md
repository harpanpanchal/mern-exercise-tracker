## Exercise Tracker (Full Stack application)

Exercise Tracker application allows the user to create a new user, create exercise log and edit / delete the exercise items from front end (React.js);

Back end can also work independently and provide the same functionality without we needing to use front end. Node.js / Express / MongoDB have been used for back end.

### Installation Guideline

1. Open terminal and run 'npm install' to install packages for front end.
2. Open 'backend' folder in terminal and run 'npm install' to install packages for backend.
3. Run 'nodemon server.js' command to run the server in the terminal which will start server at port 5000 or any of the available ports if not at 5000.
4. Open another terminal and inside the main directory, run 'npm start' to run the front end. It would then open the http://localhost:3000.

### Important points

1. Back end: Node.js / Express / MongoDB
2. Front end: React.js
